package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type task struct {
	ID      int    `json:ID`
	Name    string `json:Name`
	Content string `json:Content`
}

type allTasks []task

var tasks = allTasks{
	{
		ID:      1,
		Name:    "Task One",
		Content: "Some Content",
	},
	{
		ID:      2,
		Name:    "Task Two",
		Content: "Some Content",
	},
}

func getTasks(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(tasks)
}

func createTasks(w http.ResponseWriter, r *http.Request) {
	var newTasks task
	reqBody, err := ioutil.ReadAll(r.Body) //Recibimos los datos

	if err != nil {
		fmt.Fprintf(w, "Insert a valid task")
	} //Validamos si existe un error

	json.Unmarshal(reqBody, &newTasks) //Si no existe un error asignamos el reqBody que es lo que recibo, al newTasks. que contiene una estructura tipo task.
	newTasks.ID = len(tasks) + 1       //Le decimos que el ID sea igual al tamaño de la longitud mas 1(eso quiere decir que si actualmente hay 2 objetos, + 1, el proximo tendrá ID 3, y así consecutivamente)
	tasks = append(tasks, newTasks)    //Lo guardamos en la en el array tasks los nuevos que recibimos

	w.Header().Set("Content-Type", "application-json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(newTasks)
}

func getTask(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	taskID, err := strconv.Atoi(vars["id"]) //Esto es para convertir el id que es un string, a un entero

	if err != nil {
		fmt.Fprintf(w, "ID inválido")
		return
	}

	for _, task := range tasks { //Recorrer elemento a elemento una lista, ¿Cual lista? la lista de las tareas (tasks), por cada uno que recorra obtendré una tarea (el task)
		if task.ID == taskID {
			w.Header().Set("Content-Type", "application-json") //Para decirle que el tipo de dato que estoy enviando en un json.
			json.NewEncoder(w).Encode(task)
		}
	}

	//vars["id"] Este nombre debe ser el mismo que está en la ruta, si en la ruta pongo otra cosa que no sea id, aqui lo debo cambiar igual.
}

func deleteTask(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r) //Con esto metemos los parametros de la ruta dentro de vars

	taskID, err := strconv.Atoi(vars["id"]) //Esto es para convertir el id que es un string, a un entero

	if err != nil {
		fmt.Fprintf(w, "ID inválido")
		return
	}

	for index, t := range tasks { //Recorrer elemento a elemento una lista, ¿Cual lista? la lista de las tareas (tasks), por cada uno que recorra obtendré una tarea (el task)
		if t.ID == taskID {
			tasks = append(tasks[:index], tasks[index+1:]...) //Esto lo que indica es, el primer valor que voy a conservar todos los elementos que vienen antes de la task que he encontrado (task[:index]) y el segundo lo que dice es que voy a conservar todos los elementos tambien que vienen despues del elemento que he encontrado (tasks[index+1:]...) Es decir, conserva todos, menos el que encuentra.
			fmt.Fprintf(w, "The task with ID %v has been removed successfully", taskID)
		}
	}
}

func updateTask(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r) //Con esto metemos los parametros de la ruta dentro de vars

	taskID, err := strconv.Atoi(vars["id"]) //Esto es para convertir el id que es un string, a un entero
	var updateTask task

	if err != nil {
		fmt.Fprintf(w, "ID inválido")
		return
	}

	reqBody, err := ioutil.ReadAll(r.Body) //Recibimos los datos

	if err != nil {
		fmt.Fprintf(w, "Please enter valid data.")
	}

	json.Unmarshal(reqBody, &updateTask)

	for i, task := range tasks {
		if task.ID == taskID {
			tasks = append(tasks[:i], tasks[i+1:]...) //eliminamos solo el que tasks i
			updateTask.ID = taskID
			tasks = append(tasks, updateTask)

			fmt.Fprintf(w, "The task with ID %v has been updated successfully.", taskID)
		}
	}
}

func indexRoute(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to my api")
}

func main() {
	router := mux.NewRouter().StrictSlash(true) //StrictSlash es modo estricto

	router.HandleFunc("/", indexRoute)
	router.HandleFunc("/tasks", getTasks).Methods("GET")
	router.HandleFunc("/tasks", createTasks).Methods("POST")
	router.HandleFunc("/tasks/{id}", getTask).Methods("GET")
	router.HandleFunc("/tasks/{id}", deleteTask).Methods("DELETE")
	router.HandleFunc("/tasks/{id}", updateTask).Methods("PUT")
	log.Fatal(http.ListenAndServe(":4000", router))
}
